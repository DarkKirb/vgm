use super::AudioExpansions;
pub struct MemoryMap {
    pub iram: [u8; 0x800],
    pub banks: Vec<[u8; 0x1000]>,
    pub bank_states: [u8; 16], // first 6/8 are unused
    pub expansions: AudioExpansions,
    pub open_bus: u8
}

impl mem::MemoryMap for MemoryMap {
    fn read_byte(&mut self, addr: u32) -> Option<u8> {
        let addr = addr as u16;
        if addr < 0x2000 {
            self.open_bus = self.iram[(addr & 0x7FF) as usize];
        }
        if addr >= 0x8000 {
            let bank = self.bank_states[(addr >> 12) as usize];
            self.open_bus = self.banks[bank as usize][(addr & 0xFFF) as usize];
        }
        if self.expansions.contains(AudioExpansions::FDS) && addr >= 0x6000 {
            let bank = self.bank_states[(addr >> 12) as usize];
            self.open_bus = self.banks[bank as usize][(addr & 0xFFF) as usize];
        }
        if addr > 0x5FF0 && addr < 0x6000 {
            self.open_bus = self.bank_states[addr as usize - 0x5FF0];
        }
        Some(self.open_bus)
    }
    fn write_byte(&mut self, addr: u32, data: u8) -> Option<()> {
        self.open_bus = data;
        Some(())
    }
}
