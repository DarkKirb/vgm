use std::num::Wrapping;

bitflags! {
    pub struct PReg: u8 {
        /// Carry flag
        const C = 0b0000_0001;
        /// Zero flag
        const Z = 0b0000_0010;
        /// Interrupt flag
        const I = 0b0000_0100;
        /// Decimal flag. Does nothing
        const D = 0b0000_1000;
        /// Break flag. Set when the interrupt was caused by brk
        const B = 0b0001_0000;
        /// Unused flag
        const UNUSED = 0b0010_0000;
        /// Overflow flag
        const V = 0b0100_0000;
        /// Negative flag
        const N = 0b1000_0000;
    }
}

#[allow(non_snake_case)]
pub struct Regset {
    pub A: Wrapping<u8>,
    pub X: Wrapping<u8>,
    pub Y: Wrapping<u8>,
    pub S: Wrapping<u8>,
    pub PC: Wrapping<u16>,
    pub P: PReg,
}

pub struct CPU<'a, MM: mem::MemoryMap> {
    pub regs: Regset,
    pub mem: &'a mut MM,
    pub ins: u8,
    pub tstate: i8,
    pub ins2: u8,
    al: u8,
    ah: u8,
    ax: u16,
    carry: bool,
}

macro_rules! read_cycle {
    ( $self: expr, $addr: expr, $assign: expr, $exec: expr, $inc_state: expr) => {{
        $assign = $self.mem.read_byte($addr).unwrap();
        $exec;
        $self.tstate += $inc_state;
    }};
}

macro_rules! write_cycle {
    ( $self: expr, $addr: expr, $set: expr, $inc_state: expr) => {{
        let value = $set;
        $self.mem.write_byte($addr, value).unwrap();
        $self.tstate += $inc_state;
    }};
}

macro_rules! push_cycle {
    ( $self: expr, $value: expr ) => {
        write_cycle!(
            $self,
            0x100 + ($self.regs.S.0 as u32),
            {
                $self.regs.S -= Wrapping(1);
                $value
            },
            1
        );
    };
    ( $self: expr, $value: expr, $tstate: expr ) => {
        write_cycle!(
            $self,
            0x100 + ($self.regs.S.0 as u32),
            {
                $self.regs.S -= Wrapping(1);
                $value
            },
            $tstate
        );
    };
}

macro_rules! pop_cycle {
    ( $self: expr, $assign: expr ) => {
        read_cycle!(
            $self,
            0x100 + ($self.regs.S.0 as u32),
            $assign,
            {
                $self.regs.S += Wrapping(1);
            },
            1
        );
    };
    ( $self: expr, $assign: expr, $exec: expr ) => {
        read_cycle!(
            $self,
            0x100 + ($self.regs.S.0 as u32),
            $assign,
            {
                $self.regs.S += Wrapping(1);
                $exec;
            },
            1
        );
    };
}

macro_rules! pop_cycle2 {
    ( $self: expr, $assign: expr ) => {
        read_cycle!($self, 0x100 + ($self.regs.S.0 as u32), $assign, {}, 1);
    };
    ( $self: expr, $assign: expr, $exec: expr ) => {
        read_cycle!($self, 0x100 + ($self.regs.S.0 as u32), $assign, $exec, 1);
    };
    ( $self: expr, $assign: expr, $exec: expr, $tstate: expr ) => {
        read_cycle!(
            $self,
            0x100 + ($self.regs.S.0 as u32),
            $assign,
            $exec,
            $tstate
        );
    };
}

macro_rules! implied_ins {
    ( $self: expr, $exec: expr ) => {
        match $self.tstate {
            1 => read_cycle!($self, $self.regs.PC.0 as u32, $self.ins2, $exec, -2),
            _ => {}
        }
    };
}

macro_rules! immediate_ins {
    ( $self: expr, $exec: expr ) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ins2,
                {
                    $self.regs.PC += Wrapping(1);
                    $exec;
                },
                -2
            ),
            _ => {}
        }
    };
}

macro_rules! abs_ro_ins {
    ( $self: expr, $exec: expr ) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ah,
                {
                    $self.regs.PC += Wrapping(1);
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                },
                1
            ),
            3 => read_cycle!($self, $self.ax as u32, $self.ins2, $exec, -4),
            _ => {}
        }
    };
}

macro_rules! abs_rw_ins {
    ( $self: expr, $exec: expr ) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ah,
                {
                    $self.regs.PC += Wrapping(1);
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                },
                1
            ),
            3 => read_cycle!($self, $self.ax as u32, $self.ins2, {}, 1),
            4 => write_cycle!($self, $self.ax as u32, $self.ins2, 1),
            5 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -6
            ),
            _ => {}
        }
    };
}

macro_rules! abs_wo_ins {
    ( $self: expr, $exec: expr ) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ah,
                {
                    $self.regs.PC += Wrapping(1);
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                },
                1
            ),
            3 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -4
            ),
            _ => {}
        }
    };
}

macro_rules! zp_ro_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.al as u32, $self.al, $exec, -3),
            _ => {}
        }
    };
}

macro_rules! zp_rw_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.al as u32, $self.ins2, {}, 1),
            3 => write_cycle!($self, $self.al as u32, $self.ins2, 1),
            4 => write_cycle!(
                $self,
                $self.al as u32,
                {
                    $exec;
                    $self.ins2
                },
                -5
            ),
            _ => {}
        }
    };
}

macro_rules! zp_wo_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.al as u32, $self.ins2, {}, 1),
            3 => write_cycle!(
                $self,
                $self.al as u32,
                {
                    $exec;
                    $self.ins2
                },
                -4
            ),
            _ => {}
        }
    };
}

macro_rules! zpi_ro_ins {
    ( $self: expr, $exec: expr, $idx: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.al = $self.al.wrapping_add($idx.0),
                1
            ),
            3 => read_cycle!($self, $self.al as u32, $self.ins2, $exec, -4),
            _ => {}
        }
    };
}

macro_rules! zpi_rw_ins {
    ( $self: expr, $exec: expr) => {
        zpi_rw_ins!($self, $exec, $self.regs.X);
    };
    ( $self: expr, $exec: expr, $reg: expr ) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.al = $self.al.wrapping_add($reg.0),
                1
            ),
            3 => read_cycle!($self, $self.al as u32, $self.ins2, {}, 1),
            4 => write_cycle!($self, $self.al as u32, $self.ins2, 1),
            5 => write_cycle!(
                $self,
                $self.al as u32,
                {
                    $exec;
                    $self.ins2
                },
                -6
            ),
            _ => {}
        }
    };
}

macro_rules! zpi_wo_ins {
    ( $self: expr, $exec: expr, $idx: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.al = $self.al.wrapping_add($idx.0),
                1
            ),
            3 => write_cycle!(
                $self,
                ($self.al as u32),
                {
                    $exec;
                    $self.ins2
                },
                1
            ),
            _ => {}
        }
    };
}

macro_rules! absi_ro_ins {
    ( $self: expr, $exec: expr, $idx: expr) => {
        match $self.tstate {
            1 => read_cycle!($self, $self.regs.PC.0 as u32, $self.al, $self.regs.PC += Wrapping(1), 1),
            2 => read_cycle!($self, $self.regs.PC.0 as u32, $self.ah, {
                $self.regs.PC += Wrapping(1);
                $self.ax = ($self.al as u16 + ($self.ah as u16) << 8).wrapping_add($idx.0 as u16); // Real address
                $self.carry = false;
                if $self.al.wrapping_add($idx.0) < $self.al {
                    $self.carry = true;
                }
            }, 1),
            3 => read_cycle!($self, $self.al.wrapping_add($idx.0) as u32 + ($self.ah as u32) << 8, $self.ins2, { if $self.carry { $exec; }}, { if $self.carry {
                1
            } else {
                -4
            }}),
            4 => read_cycle!($self, $self.ax as u32, $self.ins2, $exec, -5),
            _ => {}
        }
    }
}

macro_rules! absi_rw_ins {
    ( $self: expr, $exec: expr) => {
        absi_rw_ins!($self, $exec, $self.regs.X);
    };
    ( $self: expr, $exec: expr, $idx: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ah,
                {
                    $self.regs.PC += Wrapping(1);
                    $self.ax = ($self.al as u16 + ($self.ah as u16) << 8)
                        .wrapping_add($idx.0 as u16); // Real address
                    $self.carry = false;
                    if $self.al.wrapping_add($idx.0) < $self.al {
                        $self.carry = true;
                    }
                },
                1
            ),
            3 => read_cycle!(
                $self,
                $self.al.wrapping_add($idx.0) as u32 + ($self.ah as u32) << 8,
                $self.ins2,
                {
                    if $self.carry {
                        $exec;
                    }
                },
                {
                    if $self.carry {
                        1
                    } else {
                        2
                    }
                }
            ),
            4 => read_cycle!($self, $self.ax as u32, $self.ins2, $exec, 1),
            5 => write_cycle!($self, $self.ax as u32, $self.ins2, 1),
            6 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -7
            ),
            _ => {}
        }
    };
}

macro_rules! absi_wo_ins {
    ( $self: expr, $exec: expr, $idx: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ah,
                {
                    $self.regs.PC += Wrapping(1);
                    $self.ax = ($self.al as u16 + ($self.ah as u16) << 8)
                        .wrapping_add($self.regs.X.0 as u16); // Real address
                },
                1
            ),
            3 => read_cycle!(
                $self,
                $self.al.wrapping_add($idx.0) as u32 + ($self.ah as u32) << 8,
                $self.ins2,
                {},
                1
            ),
            4 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -5
            ),
            _ => {}
        }
    };
}

macro_rules! pc_rel_ins {
    ( $self: expr, $cond: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ins2,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.regs.PC.0 as u32, $self.al, {}, {
                if $cond {
                    1
                } else {
                    -3
                }
            }),
            3 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                {
                    if $self.ins2 >= 0x80 {
                        // it's negative, subtract ~$self.ins2+1 from PC
                        let real = $self.regs.PC - Wrapping((!$self.ins2 + 1) as u16);
                        $self.regs.PC = Wrapping(
                            $self.al.wrapping_sub(!$self.ins2 + 1) as u16 + ($self.ah as u16) << 8,
                        );
                        $self.carry = $self.regs.PC != real;
                    } else {
                        let real = $self.regs.PC + Wrapping($self.ins2 as u16);
                        $self.regs.PC = Wrapping(
                            $self.al.wrapping_add($self.ins2) as u16 + ($self.ah as u16) << 8,
                        );
                        $self.carry = $self.regs.PC != real;
                    }
                    if !$self.carry {
                        $self.regs.PC += Wrapping(1);
                    }
                },
                {
                    if $self.carry {
                        1
                    } else {
                        -4
                    }
                }
            ),
            4 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                {
                    if $self.ins2 >= 0x80 {
                        $self.regs.PC -= Wrapping((!$self.ins2 + 1) as u16);
                    } else {
                        $self.regs.PC += Wrapping($self.ins2 as u16);
                    }
                    $self.regs.PC += Wrapping(1);
                },
                -5
            ),
            _ => {}
        }
    };
}

macro_rules! idx_ind_ro_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.ins2 = $self.al.wrapping_add($self.regs.X.0),
                1
            ),
            3 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            4 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                $self.ax = $self.al as u16 + ($self.ah as u16) << 8,
                1
            ),
            5 => read_cycle!($self, $self.ax as u32, $self.ins2, $exec, -6),
            _ => {}
        }
    };
}

macro_rules! idx_ind_rw_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.ins2 = $self.al.wrapping_add($self.regs.X.0),
                1
            ),
            3 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            4 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                $self.ax = $self.al as u16 + ($self.ah as u16) << 8,
                1
            ),
            5 => read_cycle!($self, $self.ax as u32, $self.ins2, $exec, 1),
            6 => write_cycle!($self, $self.ax as u32, $self.ins2, 1),
            7 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -8
            ),
            _ => {}
        }
    };
}

macro_rules! idx_ind_wo_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.al,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!(
                $self,
                $self.al as u32,
                $self.ins2,
                $self.ins2 = $self.al.wrapping_add($self.regs.X.0),
                1
            ),
            3 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            4 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                $self.ax = $self.al as u16 + ($self.ah as u16) << 8,
                1
            ),
            5 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -6
            ),
            _ => {}
        }
    };
}

macro_rules! ind_idx_ro_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ins2,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            3 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                {
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                    $self.carry = false;
                    if $self.al.wrapping_add($self.regs.Y.0) < $self.al {
                        $self.carry = true;
                    }
                },
                1
            ),
            4 => read_cycle!(
                $self,
                $self.al.wrapping_add($self.regs.Y.0) as u32 + ($self.ah as u32) << 8,
                $self.ins2,
                {
                    if !$self.carry {
                        $exec;
                    }
                },
                {
                    if $self.carry {
                        1
                    } else {
                        -5
                    }
                }
            ),
            5 => read_cycle!(
                $self,
                $self.ax.wrapping_add($self.regs.Y.0 as u16) as u32,
                $self.ins2,
                $exec,
                -6
            ),
            _ => {}
        }
    };
}

macro_rules! ind_idx_rw_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ins2,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            3 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                {
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                    $self.ax = $self.ax.wrapping_add($self.regs.Y.0 as u16)
                },
                1
            ),
            4 => read_cycle!(
                $self,
                $self.al.wrapping_add($self.regs.Y.0) as u32 + ($self.ah as u32) << 8,
                $self.ins2,
                {},
                1
            ),
            5 => read_cycle!($self, $self.ax as u32, $self.ins2, {}, 1),
            6 => write_cycle!($self, $self.ax as u32, $self.ins2, 1),
            7 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -8
            ),
            _ => {}
        }
    };
}

macro_rules! ind_idx_wo_ins {
    ( $self: expr, $exec: expr) => {
        match $self.tstate {
            1 => read_cycle!(
                $self,
                $self.regs.PC.0 as u32,
                $self.ins2,
                $self.regs.PC += Wrapping(1),
                1
            ),
            2 => read_cycle!($self, $self.ins2 as u32, $self.al, {}, 1),
            3 => read_cycle!(
                $self,
                $self.ins2.wrapping_add(1) as u32,
                $self.ah,
                {
                    $self.ax = $self.al as u16 + ($self.ah as u16) << 8;
                    $self.ax = $self.ax.wrapping_add($self.regs.Y.0 as u16)
                },
                1
            ),
            4 => read_cycle!(
                $self,
                $self.al.wrapping_add($self.regs.Y.0) as u32 + ($self.ah as u32) << 8,
                $self.ins2,
                {},
                1
            ),
            5 => write_cycle!(
                $self,
                $self.ax as u32,
                {
                    $exec;
                    $self.ins2
                },
                -6
            ),
            _ => {}
        }
    };
}

impl<'a, MM: mem::MemoryMap> CPU<'a, MM> {
    pub fn new(mem: &'a mut MM) -> CPU<'a, MM> {
        CPU {
            regs: Regset {
                A: Wrapping(0),
                X: Wrapping(0),
                Y: Wrapping(0),
                S: Wrapping(0),
                PC: Wrapping(mem.read_little::<u16>(0xFFFC).unwrap()),
                P: PReg::UNUSED | PReg::I,
            },
            mem,
            ins: 0,
            tstate: -1, // No instruction run
            ins2: 0,
            al: 0,
            ah: 0,
            ax: 0,
            carry: false,
        }
    }
    /// Runs for one clock cycle.
    pub fn tick(&mut self) {
        if self.tstate < 0 {
            self.ins = self.mem.read_byte(self.regs.PC.0 as u32).unwrap();
            self.regs.PC += Wrapping(1);
            self.tstate = 1;
            return;
        }
        match self.ins {
            0x00 => {
                // BRK
                match self.tstate {
                    1 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.ins2,
                        self.regs.PC += Wrapping(1),
                        1
                    ),
                    2 => push_cycle!(self, (self.regs.PC.0 >> 8) as u8),
                    3 => push_cycle!(self, self.regs.PC.0 as u8),
                    4 => push_cycle!(self, (self.regs.P | PReg::B).bits()),
                    5 => read_cycle!(self, 0xFFFE, self.al, {}, 1),
                    6 => read_cycle!(
                        self,
                        0xFFFF,
                        self.ah,
                        self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8),
                        -7
                    ),
                    _ => {}
                }
            }
            0x01 => {
                // ORA (d, x)
                idx_ind_ro_ins!(self, self.run_ora());
            }
            0x02 | 0x12 | 0x22 | 0x32 | 0x42 | 0x52 | 0x62 | 0x72 | 0x92 | 0xB2 | 0xD2 | 0xF2 => {
                // KIL
                panic!("KIL instruction run!");
            }
            0x03 => {
                // SLO (d, x) - shifts memory address left, stores the result, and ors it with A
                idx_ind_rw_ins!(self, self.run_slo());
            }
            0x04 => {
                // NOP d - 3 cycle NOP
                zp_ro_ins!(self, {});
            }
            0x05 => {
                // ORA d
                zp_ro_ins!(self, self.run_ora());
            }
            0x06 => {
                // ASL d
                zp_rw_ins!(self, self.run_asl());
            }
            0x07 => {
                // SLO d
                zp_rw_ins!(self, self.run_slo());
            }
            0x08 => {
                // PHP
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => push_cycle!(self, self.regs.P.bits(), -3),
                    _ => {}
                }
            }
            0x09 => {
                // ORA #1
                immediate_ins!(self, self.run_ora());
            }
            0x0A => {
                // ASL
                implied_ins!(self, {
                    self.ins2 = self.regs.A.0;
                    self.run_asl();
                    self.regs.A = Wrapping(self.ins2);
                });
            }
            0x0B | 0x2B => {
                // ANC #i
                immediate_ins!(self, self.run_anc());
            }
            0x0C => {
                // NOP a - 4 cycle NOP
                abs_ro_ins!(self, {});
            }
            0x0D => {
                // ORA a
                abs_ro_ins!(self, self.run_ora());
            }
            0x0E => {
                // ASL a
                abs_rw_ins!(self, self.run_asl());
            }
            0x0F => {
                // SLO a
                abs_rw_ins!(self, self.run_slo());
            }
            0x10 => {
                // BPL
                pc_rel_ins!(self, !self.regs.P.contains(PReg::N));
            }
            0x11 => {
                // ORA (d), y
                ind_idx_ro_ins!(self, self.run_ora());
            }
            0x13 => {
                // SLO (d), y
                ind_idx_rw_ins!(self, self.run_slo());
            }
            0x14 => {
                // NOP d, x
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0x15 => {
                // ORA d, x
                zpi_ro_ins!(self, self.run_ora(), self.regs.X);
            }
            0x16 => {
                // ASL d, x
                zpi_rw_ins!(self, self.run_asl());
            }
            0x17 => {
                // SLO d, x
                zpi_rw_ins!(self, self.run_slo());
            }
            0x18 => {
                // CLC
                implied_ins!(self, self.regs.P.remove(PReg::C));
            }
            0x19 => {
                // ORA a, y
                zpi_ro_ins!(self, self.run_ora(), self.regs.Y);
            }
            0x1A | 0x3A | 0x5A | 0x7A | 0xDA | 0xEA | 0xFA => {
                // NOP
                implied_ins!(self, {});
            }
            0x1B => {
                // SLO a, y
                absi_rw_ins!(self, self.run_slo(), self.regs.Y);
            }
            0x1C => {
                // NOP a, x (4-5 cycle NOP)
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0x1D => {
                // ORA a, x
                absi_ro_ins!(self, self.run_ora(), self.regs.X);
            }
            0x1E => {
                // ASL a, x
                absi_rw_ins!(self, self.run_asl(), self.regs.X);
            }
            0x1F => {
                // SLO a, x
                absi_rw_ins!(self, self.run_slo(), self.regs.X);
            }
            0x20 => {
                // JSR
                match self.tstate {
                    1 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.al,
                        self.regs.PC += Wrapping(1),
                        1
                    ),
                    2 => pop_cycle2!(self, self.ins2), // IDK what it does in this cycle
                    3 => push_cycle!(self, (self.regs.PC.0 >> 8) as u8),
                    4 => push_cycle!(self, self.regs.PC.0 as u8),
                    5 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.ah,
                        self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8),
                        -6
                    ),
                    _ => {}
                }
            }
            0x21 => {
                // AND (d, x)
                idx_ind_ro_ins!(self, self.run_and());
            }
            0x23 => {
                // RLA (d, x)
                idx_ind_rw_ins!(self, self.run_rla());
            }
            0x24 => {
                // BIT d
                zp_ro_ins!(self, self.run_bit());
            }
            0x25 => {
                // AND d
                zp_ro_ins!(self, self.run_and());
            }
            0x26 => {
                // ROL z
                zp_rw_ins!(self, self.run_rol());
            }
            0x27 => {
                // RLA z
                zp_rw_ins!(self, self.run_rla());
            }
            0x28 => {
                // PLP
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => pop_cycle!(self, self.ins2),
                    3 => pop_cycle2!(
                        self,
                        self.ins2,
                        {
                            self.regs.P = PReg::from_bits_truncate(self.ins2);
                        },
                        -4
                    ),
                    _ => {}
                }
            }
            0x29 => {
                // AND #i
                immediate_ins!(self, self.run_and());
            }
            0x2A => {
                // ROL
                implied_ins!(self, {
                    self.ins2 = self.regs.A.0;
                    self.run_rol();
                    self.regs.A = Wrapping(self.ins2);
                });
            }
            0x2C => {
                // BIT a
                abs_ro_ins!(self, self.run_bit());
            }
            0x2D => {
                // AND a
                abs_ro_ins!(self, self.run_and());
            }
            0x2E => {
                // ROL a
                abs_rw_ins!(self, self.run_rol());
            }
            0x2F => {
                // RLA a
                abs_rw_ins!(self, self.run_rla());
            }
            0x30 => {
                // BMI
                pc_rel_ins!(self, self.regs.P.contains(PReg::N));
            }
            0x31 => {
                // AND (z), y
                ind_idx_ro_ins!(self, self.run_and());
            }
            0x33 => {
                // RLA (z), y
                ind_idx_rw_ins!(self, self.run_rla());
            }
            0x34 => {
                // NOP z, x (4-cycle nop)
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0x35 => {
                // AND z, x
                zpi_ro_ins!(self, self.run_and(), self.regs.X);
            }
            0x36 => {
                // ROL z, x
                zpi_rw_ins!(self, self.run_rol());
            }
            0x37 => {
                // RLA z, x
                zpi_rw_ins!(self, self.run_rla());
            }
            0x38 => {
                // SEC
                implied_ins!(self, self.regs.P.insert(PReg::C));
            }
            0x39 => {
                // AND a, y
                absi_ro_ins!(self, self.run_and(), self.regs.Y);
            }
            0x3B => {
                // RLA a, y
                absi_rw_ins!(self, self.run_rla(), self.regs.Y);
            }
            0x3C => {
                // NOP a, x (4 cycle)
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0x3D => {
                // AND a, x
                absi_ro_ins!(self, self.run_and(), self.regs.X);
            }
            0x3E => {
                // ROL a, x
                absi_rw_ins!(self, self.run_rol(), self.regs.X);
            }
            0x3F => {
                // RLA a, x
                absi_rw_ins!(self, self.run_rla(), self.regs.X);
            }
            0x40 => {
                // RTI
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => pop_cycle!(self, self.ins2),
                    3 => pop_cycle!(self, self.ins2, {
                        self.regs.P = PReg::from_bits_truncate(self.ins2);
                    }),
                    4 => pop_cycle!(self, self.al),
                    5 => pop_cycle2!(
                        self,
                        self.ah,
                        {
                            self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8);
                        },
                        -6
                    ),
                    _ => {}
                }
            }
            0x41 => {
                // EOR (z, x)
                idx_ind_ro_ins!(self, self.run_eor());
            }
            0x43 => {
                // SRE (z, x)
                idx_ind_rw_ins!(self, self.run_sre());
            }
            0x44 => {
                // NOP z
                zp_ro_ins!(self, {});
            }
            0x45 => {
                // EOR z
                zp_ro_ins!(self, self.run_eor());
            }
            0x46 => {
                // LSR z
                zp_rw_ins!(self, self.run_lsr());
            }
            0x47 => {
                // SRE z
                zp_rw_ins!(self, self.run_sre());
            }
            0x48 => {
                // PHA
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => push_cycle!(self, self.regs.A.0, -3),
                    _ => {}
                }
            }
            0x49 => {
                // EOR #i
                immediate_ins!(self, self.run_eor());
            }
            0x4A => {
                // LSR
                implied_ins!(self, {
                    self.ins2 = self.regs.A.0;
                    self.run_lsr();
                    self.regs.A = Wrapping(self.ins2);
                });
            }
            0x4B => {
                // ALR #i
                immediate_ins!(self, self.run_alr());
            }
            0x4C => {
                // JMP a
                match self.tstate {
                    1 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.al,
                        self.regs.PC += Wrapping(1),
                        1
                    ),
                    2 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.ah,
                        self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8),
                        -3
                    ),
                    _ => {}
                }
            }
            0x4D => {
                // EOR a
                abs_ro_ins!(self, self.run_eor());
            }
            0x4E => {
                // LSR a
                abs_rw_ins!(self, self.run_lsr());
            }
            0x4F => {
                // SRE a
                abs_rw_ins!(self, self.run_sre());
            }
            0x50 => {
                // BVC
                pc_rel_ins!(self, !self.regs.P.contains(PReg::V));
            }
            0x51 => {
                // EOR (z), y
                ind_idx_ro_ins!(self, self.run_eor());
            }
            0x53 => {
                // SRE (z), y
                ind_idx_rw_ins!(self, self.run_sre());
            }
            0x54 => {
                // NOP z, x
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0x55 => {
                // EOR z, x
                zpi_ro_ins!(self, self.run_eor(), self.regs.X);
            }
            0x56 => {
                // LSR z, x
                zpi_rw_ins!(self, self.run_lsr());
            }
            0x57 => {
                // SRE z, x
                zpi_rw_ins!(self, self.run_sre());
            }
            0x58 => {
                // CLI
                implied_ins!(self, self.regs.P.remove(PReg::I));
            }
            0x59 => {
                // EOR a, y
                absi_ro_ins!(self, self.run_eor(), self.regs.Y);
            }
            0x5B => {
                // SRE a, y
                absi_rw_ins!(self, self.run_sre(), self.regs.Y);
            }
            0x5C => {
                // NOP a, x
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0x5D => {
                // EOR a, x
                absi_ro_ins!(self, self.run_eor(), self.regs.X);
            }
            0x5E => {
                // LSR a, x
                absi_rw_ins!(self, self.run_lsr(), self.regs.X);
            }
            0x5F => {
                // SRE a, x
                absi_rw_ins!(self, self.run_sre(), self.regs.X);
            }
            0x60 => {
                // RTS
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => pop_cycle!(self, self.ins2),
                    3 => pop_cycle!(self, self.al),
                    4 => pop_cycle2!(self, self.ah, {
                        self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8);
                    }),
                    5 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.ins2,
                        self.regs.PC += Wrapping(1),
                        -6
                    ),
                    _ => {}
                }
            }
            0x61 => {
                // ADC (z, x)
                idx_ind_ro_ins!(self, self.run_adc());
            }
            0x63 => {
                // RRA (z, x)
                idx_ind_rw_ins!(self, self.run_rra());
            }
            0x64 => {
                // NOP z
                zp_ro_ins!(self, {});
            }
            0x65 => {
                // ADC z
                zp_ro_ins!(self, self.run_adc());
            }
            0x66 => {
                // ROR z
                zp_rw_ins!(self, self.run_ror());
            }
            0x67 => {
                // RRA z
                zp_rw_ins!(self, self.run_rra());
            }
            0x68 => {
                // PLA
                match self.tstate {
                    1 => read_cycle!(self, self.regs.PC.0 as u32, self.ins2, {}, 1),
                    2 => pop_cycle!(self, self.ins2),
                    3 => pop_cycle2!(
                        self,
                        self.ins2,
                        {
                            self.regs.A = Wrapping(self.ins2);
                        },
                        -4
                    ),
                    _ => {}
                }
            }
            0x69 => {
                // ADC #i
                immediate_ins!(self, self.run_adc());
            }
            0x6A => {
                // ROR
                implied_ins!(self, {
                    self.ins2 = self.regs.X.0;
                    self.run_ror();
                    self.regs.X = Wrapping(self.ins2);
                });
            }
            0x6B => {
                // ARR #i
                immediate_ins!(self, self.run_arr());
            }
            0x6C => {
                // JMP (a)
                match self.tstate {
                    1 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.al,
                        self.regs.PC += Wrapping(1),
                        1
                    ),
                    2 => read_cycle!(
                        self,
                        self.regs.PC.0 as u32,
                        self.ah,
                        self.ax = self.al as u16 + (self.ah as u16) << 8,
                        1
                    ),
                    3 => read_cycle!(self, self.ax as u32, self.al, {}, 1),
                    4 => read_cycle!(
                        self,
                        self.ax.wrapping_add(1) as u32,
                        self.ah,
                        self.regs.PC = Wrapping(self.al as u16 + (self.ah as u16) << 8),
                        -5
                    ),
                    _ => {}
                }
            }
            0x6D => {
                // ADC a
                abs_ro_ins!(self, self.run_adc());
            }
            0x6E => {
                // ROR a
                abs_rw_ins!(self, self.run_ror());
            }
            0x6F => {
                // RRA a
                abs_rw_ins!(self, self.run_rra());
            }
            0x70 => {
                // BVS
                pc_rel_ins!(self, self.regs.P.contains(PReg::V));
            }
            0x71 => {
                // ADC (z), y
                ind_idx_ro_ins!(self, self.run_adc());
            }
            0x73 => {
                // RRA (z), y
                ind_idx_rw_ins!(self, self.run_rra());
            }
            0x74 => {
                // NOP z, x
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0x75 => {
                // ADC z, x
                zpi_ro_ins!(self, self.run_adc(), self.regs.X);
            }
            0x76 => {
                // ROR z, x
                zpi_rw_ins!(self, self.run_ror());
            }
            0x77 => {
                // RRA z, x
                zpi_rw_ins!(self, self.run_rra());
            }
            0x78 => {
                // CLI
                implied_ins!(self, self.regs.P.insert(PReg::I));
            }
            0x79 => {
                // ADC z, y
                zpi_ro_ins!(self, self.run_adc(), self.regs.Y);
            }
            0x7B => {
                // RRA z, y
                zpi_rw_ins!(self, self.run_rra(), self.regs.Y);
            }
            0x7C => {
                // NOP a, x
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0x7D => {
                // ADC a, x
                absi_ro_ins!(self, self.run_adc(), self.regs.X);
            }
            0x7E => {
                // ROR a, x
                absi_rw_ins!(self, self.run_ror());
            }
            0x7F => {
                // RRA a, x
                absi_rw_ins!(self, self.run_rra());
            }
            0x80 | 0x82 | 0x89 | 0xC2 | 0xE2 => {
                // NOP #i
                immediate_ins!(self, {});
            }
            0x81 => {
                // STA (z, x)
                idx_ind_wo_ins!(self, self.run_sta());
            }
            0x83 => {
                // SAX (z, x)
                idx_ind_wo_ins!(self, self.run_sax());
            }
            0x84 => {
                // STY z
                zp_wo_ins!(self, self.run_sty());
            }
            0x85 => {
                // STA z
                zp_wo_ins!(self, self.run_sta());
            }
            0x86 => {
                // STX z
                zp_wo_ins!(self, self.run_stx());
            }
            0x87 => {
                // SAX z
                zp_wo_ins!(self, self.run_sax());
            }
            0x88 => {
                // DEY
                implied_ins!(self, self.run_dey());
            }
            0x8A => {
                // TXA
                implied_ins!(self, self.run_txa());
            }
            0x8B => {
                // XAA #i
                immediate_ins!(self, self.run_xaa());
            }
            0x8C => {
                // STY a
                abs_wo_ins!(self, self.run_sty());
            }
            0x8D => {
                // STA a
                abs_wo_ins!(self, self.run_sta());
            }
            0x8E => {
                // STX a
                abs_wo_ins!(self, self.run_stx());
            }
            0x8F => {
                // SAX a
                abs_wo_ins!(self, self.run_sax());
            }
            0x90 => {
                // BCC
                pc_rel_ins!(self, !self.regs.P.contains(PReg::C));
            }
            0x91 => {
                // STA (z), y
                ind_idx_wo_ins!(self, self.run_sta());
            }
            0x93 => {
                // AHX (z), y
                ind_idx_wo_ins!(self, self.run_ahx());
            }
            0x94 => {
                // STY z, x
                zpi_wo_ins!(self, self.run_sty(), self.regs.X);
            }
            0x95 => {
                // STA z, x
                zpi_wo_ins!(self, self.run_sta(), self.regs.X);
            }
            0x96 => {
                // STX z, y
                zpi_wo_ins!(self, self.run_stx(), self.regs.Y);
            }
            0x97 => {
                // SAX z, y
                zpi_wo_ins!(self, self.run_sax(), self.regs.Y);
            }
            0x98 => {
                // TYA
                implied_ins!(self, self.run_tya());
            }
            0x99 => {
                // STA a, y
                absi_wo_ins!(self, self.run_sta(), self.regs.Y);
            }
            0x9A => {
                // TXS
                implied_ins!(self, self.regs.S = self.regs.X);
            }
            0x9B => {
                // TAS a, y
                absi_wo_ins!(self, self.run_tas(), self.regs.Y);
            }
            0x9C => {
                // SHY a, x
                absi_wo_ins!(self, self.run_shy(), self.regs.X);
            }
            0x9D => {
                // STA a, x
                absi_wo_ins!(self, self.run_sta(), self.regs.X);
            }
            0x9E => {
                // SHX a, y
                absi_wo_ins!(self, self.run_shx(), self.regs.Y);
            }
            0x9F => {
                // AHX a, y
                absi_wo_ins!(self, self.run_ahx(), self.regs.Y);
            }
            0xA0 => {
                // LDY #i
                immediate_ins!(self, self.run_ldy());
            }
            0xA1 => {
                // LDA (z, x)
                idx_ind_ro_ins!(self, self.run_lda());
            }
            0xA2 => {
                // LDX #i
                immediate_ins!(self, self.run_ldx());
            }
            0xA3 => {
                // LAX (z, x)
                idx_ind_ro_ins!(self, self.run_lax());
            }
            0xA4 => {
                // LDY z
                zp_ro_ins!(self, self.run_ldy());
            }
            0xA5 => {
                // LDA z
                zp_ro_ins!(self, self.run_lda());
            }
            0xA6 => {
                // LDX z
                zp_ro_ins!(self, self.run_ldx());
            }
            0xA7 => {
                // LAX z
                zp_ro_ins!(self, self.run_lax());
            }
            0xA8 => {
                // TAY
                implied_ins!(self, self.run_tay());
            }
            0xA9 => {
                // LDA #i
                immediate_ins!(self, self.run_lda());
            }
            0xAA => {
                // TAX
                implied_ins!(self, self.run_tax());
            }
            0xAB => {
                // LAX #i
                implied_ins!(self, self.run_lax());
            }
            0xAC => {
                // LDY a
                abs_ro_ins!(self, self.run_ldy());
            }
            0xAD => {
                // LDA a
                abs_ro_ins!(self, self.run_lda());
            }
            0xAE => {
                // LDX a
                abs_ro_ins!(self, self.run_ldx());
            }
            0xAF => {
                // LAX a
                abs_ro_ins!(self, self.run_lax());
            }
            0xB0 => {
                // BCS
                pc_rel_ins!(self, self.regs.P.contains(PReg::C));
            }
            0xB1 => {
                // LDA (z), y
                ind_idx_ro_ins!(self, self.run_lda());
            }
            0xB3 => {
                // LAX (z), y
                ind_idx_ro_ins!(self, self.run_lax());
            }
            0xB4 => {
                // LDY z, x
                zpi_ro_ins!(self, self.run_ldy(), self.regs.X);
            }
            0xB5 => {
                // LDA z, x
                zpi_ro_ins!(self, self.run_lda(), self.regs.X);
            }
            0xB6 => {
                // LDX z, y
                zpi_ro_ins!(self, self.run_ldx(), self.regs.Y);
            }
            0xB7 => {
                // LAX z, y
                zpi_ro_ins!(self, self.run_lax(), self.regs.Y);
            }
            0xB8 => {
                // CLV
                implied_ins!(self, self.regs.P.remove(PReg::V));
            }
            0xB9 => {
                // LDA a, y
                absi_ro_ins!(self, self.run_lda(), self.regs.Y);
            }
            0xBA => {
                // TSX
                implied_ins!(self, self.run_tsx());
            }
            0xBB => {
                // LAS a, y
                absi_ro_ins!(self, self.run_las(), self.regs.Y);
            }
            0xBC => {
                // LDY a, x
                absi_ro_ins!(self, self.run_ldy(), self.regs.X);
            }
            0xBD => {
                // LDA a, x
                absi_ro_ins!(self, self.run_lda(), self.regs.X);
            }
            0xBE => {
                // LDX a, y
                absi_ro_ins!(self, self.run_ldx(), self.regs.Y);
            }
            0xBF => {
                // LAX a, y
                absi_ro_ins!(self, self.run_lax(), self.regs.Y);
            }
            0xC0 => {
                // CPY #i
                immediate_ins!(self, self.run_cpy());
            }
            0xC1 => {
                // CMP (z, x)
                idx_ind_ro_ins!(self, self.run_cmp());
            }
            0xC3 => {
                // DCP (z, x)
                idx_ind_rw_ins!(self, self.run_dcp());
            }
            0xC4 => {
                // CPY z
                zp_ro_ins!(self, self.run_cpy());
            }
            0xC5 => {
                // CMP z
                zp_ro_ins!(self, self.run_cmp());
            }
            0xC6 => {
                // DEC z
                zp_rw_ins!(self, self.run_dec());
            }
            0xC7 => {
                // DCP z
                zp_rw_ins!(self, self.run_dcp());
            }
            0xC8 => {
                // INY
                implied_ins!(self, self.run_iny());
            }
            0xC9 => {
                // CMP #i
                immediate_ins!(self, self.run_cmp());
            }
            0xCA => {
                // DEX
                implied_ins!(self, self.run_dex());
            }
            0xCB => {
                // AXS #i
                immediate_ins!(self, self.run_axs());
            }
            0xCC => {
                // CPY a
                abs_ro_ins!(self, self.run_cpy());
            }
            0xCD => {
                // CMP a
                abs_ro_ins!(self, self.run_cmp());
            }
            0xCE => {
                // DEC a
                abs_rw_ins!(self, self.run_dec());
            }
            0xCF => {
                // DCP a
                abs_rw_ins!(self, self.run_dcp());
            }
            0xD0 => {
                // BNE
                pc_rel_ins!(self, !self.regs.P.contains(PReg::Z));
            }
            0xD1 => {
                // CMP (z), y
                ind_idx_ro_ins!(self, self.run_cmp());
            }
            0xD3 => {
                // DCP (z), y
                ind_idx_rw_ins!(self, self.run_dcp());
            }
            0xD4 => {
                // NOP z, x
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0xD5 => {
                // CMP z, x
                zpi_ro_ins!(self, self.run_cmp(), self.regs.X);
            }
            0xD6 => {
                // DEC z, x
                zpi_rw_ins!(self, self.run_dec());
            }
            0xD7 => {
                // DCP z, y
                zpi_rw_ins!(self, self.run_dcp());
            }
            0xD8 => {
                // CLD
                implied_ins!(self, self.regs.P.remove(PReg::D));
            }
            0xD9 => {
                // CMP a, y
                absi_ro_ins!(self, self.run_cmp(), self.regs.Y);
            }
            0xDB => {
                // DCP a, y
                absi_rw_ins!(self, self.run_dcp(), self.regs.Y);
            }
            0xDC => {
                // NOP a, x
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0xDD => {
                // CMP a, x
                absi_ro_ins!(self, self.run_cmp(), self.regs.X);
            }
            0xDE => {
                // DEC a, x
                absi_rw_ins!(self, self.run_dec(), self.regs.X);
            }
            0xDF => {
                // DCP a, x
                absi_rw_ins!(self, self.run_dcp(), self.regs.X);
            }
            0xE0 => {
                // CPX #i
                immediate_ins!(self, self.run_cpx());
            }
            0xE1 => {
                // SBC (z, x)
                idx_ind_ro_ins!(self, self.run_sbc());
            }
            0xE3 => {
                // ISC (z, x)
                idx_ind_rw_ins!(self, self.run_isc());
            }
            0xE4 => {
                // CPX z
                zp_ro_ins!(self, self.run_cpx());
            }
            0xE5 => {
                // SBC z
                zp_ro_ins!(self, self.run_sbc());
            }
            0xE6 => {
                // INC z
                zp_rw_ins!(self, self.run_inc());
            }
            0xE7 => {
                // ISC z
                zp_rw_ins!(self, self.run_isc());
            }
            0xE8 => {
                // INX
                implied_ins!(self, self.run_inx());
            }
            0xE9 | 0xEB => {
                // SBC #i
                immediate_ins!(self, self.run_sbc());
            }
            0xEC => {
                // CPX a
                abs_ro_ins!(self, self.run_cpx());
            }
            0xED => {
                // SBC a
                abs_ro_ins!(self, self.run_sbc());
            }
            0xEE => {
                // INC a
                abs_rw_ins!(self, self.run_inc());
            }
            0xEF => {
                // ISC a
                abs_rw_ins!(self, self.run_isc());
            }
            0xF0 => {
                // BEQ
                pc_rel_ins!(self, self.regs.P.contains(PReg::Z));
            }
            0xF1 => {
                // SBC (z), y
                ind_idx_ro_ins!(self, self.run_sbc());
            }
            0xF3 => {
                // ISC (z), y
                ind_idx_rw_ins!(self, self.run_isc());
            }
            0xF4 => {
                // NOP z, x
                zpi_ro_ins!(self, {}, self.regs.X);
            }
            0xF5 => {
                // SBC z, x
                zpi_ro_ins!(self, self.run_sbc(), self.regs.X);
            }
            0xF6 => {
                // INC z, x
                zpi_rw_ins!(self, self.run_inc());
            }
            0xF7 => {
                // ISC z, x
                zpi_rw_ins!(self, self.run_isc());
            }
            0xF8 => {
                // SED
                implied_ins!(self, self.regs.P.insert(PReg::D)); // Essentially a no-op
            }
            0xF9 => {
                // SBC a, y
                absi_ro_ins!(self, self.run_sbc(), self.regs.Y);
            }
            0xFB => {
                // ISC a, y
                absi_rw_ins!(self, self.run_isc(), self.regs.Y);
            }
            0xFC => {
                // NOP a, x
                absi_ro_ins!(self, {}, self.regs.X);
            }
            0xFD => {
                // SBC a, x
                absi_ro_ins!(self, self.run_sbc(), self.regs.X);
            }
            0xFE => {
                // INC a, x
                absi_rw_ins!(self, self.run_inc());
            }
            0xFF => {
                // ISC a, x
                absi_rw_ins!(self, self.run_isc());
            }
        }
    }
    fn run_ora(&mut self) {
        self.regs.A |= Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_slo(&mut self) {
        self.run_asl();
        self.run_ora();
    }
    fn run_asl(&mut self) {
        let rotated = self.ins2.rotate_left(1);
        self.ins2 = rotated & 0xFE;
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
        self.regs.P.set(PReg::C, rotated & 1 == 1);
    }
    fn run_anc(&mut self) {
        let orig = self.ins2;
        self.run_asl();
        self.ins2 = orig;
        self.run_and();
    }
    fn run_and(&mut self) {
        self.regs.A &= Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_rla(&mut self) {
        self.run_rol();
        self.run_and();
    }
    fn run_rol(&mut self) {
        let mut rotated = self.ins2.rotate_left(1);
        let carry = rotated & 1 == 1;
        if carry != self.regs.P.contains(PReg::C) {
            rotated ^= 1;
        }
        self.ins2 = rotated;
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
        self.regs.P.set(PReg::C, carry);
    }
    fn run_bit(&mut self) {
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
        self.regs.P.set(PReg::V, self.ins2 & 0x40 == 0x40);
        self.regs.P.set(PReg::Z, self.ins2 & self.regs.A.0 == 0);
    }
    fn run_eor(&mut self) {
        self.regs.A ^= Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_sre(&mut self) {
        self.run_lsr();
        self.run_eor();
    }
    fn run_lsr(&mut self) {
        self.regs.P.set(PReg::C, self.ins2 & 1 == 1);
        let rotated = self.ins2.rotate_right(1);
        self.ins2 = rotated & 0x7F;
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_alr(&mut self) {
        self.run_and();
        self.run_lsr();
    }
    fn run_adc(&mut self) {
        let old_n = self.regs.P.contains(PReg::N);
        if self.regs.P.contains(PReg::C) {
            self.ins2 = self.ins2.wrapping_add(1);
        }
        self.regs
            .P
            .set(PReg::C, self.regs.A.0.checked_add(self.ins2).is_none());
        self.regs.A += Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
        self.regs
            .P
            .set(PReg::V, old_n != self.regs.P.contains(PReg::N));
    }
    fn run_sbc(&mut self) {
        let old_n = self.regs.P.contains(PReg::N);
        if !self.regs.P.contains(PReg::C) {
            self.ins2 = self.ins2.wrapping_sub(1);
        }
        self.regs
            .P
            .set(PReg::C, self.regs.A.0.checked_sub(self.ins2).is_none());
        self.regs.A -= Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
        self.regs
            .P
            .set(PReg::V, old_n != self.regs.P.contains(PReg::N));
    }
    fn run_rra(&mut self) {
        self.run_ror();
        self.run_adc();
    }
    fn run_ror(&mut self) {
        let carry = self.ins2 & 1 == 1;
        if carry != self.regs.P.contains(PReg::C) {
            self.ins2 ^= 1;
        }
        self.ins2 = self.ins2.rotate_right(1);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
        self.regs.P.set(PReg::C, carry);
    }
    fn run_arr(&mut self) {
        self.run_and();
        self.ins2 = self.regs.A.0;
        self.run_ror();
        self.regs.A = Wrapping(self.ins2);
    }
    fn run_sta(&mut self) {
        self.ins2 = self.regs.A.0;
    }
    fn run_stx(&mut self) {
        self.ins2 = self.regs.X.0;
    }
    fn run_sty(&mut self) {
        self.ins2 = self.regs.A.0;
    }
    fn run_sax(&mut self) {
        self.ins2 = self.regs.A.0 & self.regs.X.0;
    }
    fn run_txa(&mut self) {
        self.regs.A = self.regs.X;
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_tya(&mut self) {
        self.regs.A = self.regs.Y;
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_tax(&mut self) {
        self.regs.X = self.regs.A;
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }
    fn run_tsx(&mut self) {
        self.regs.X = self.regs.S;
        self.regs.P.set(PReg::Z, self.regs.S.0 == 0);
        self.regs.P.set(PReg::N, self.regs.S.0 >= 0x80);
    }

    fn run_tay(&mut self) {
        self.regs.Y = self.regs.A;
        self.regs.P.set(PReg::Z, self.regs.A.0 == 0);
        self.regs.P.set(PReg::N, self.regs.A.0 >= 0x80);
    }

    fn run_xaa(&mut self) {
        self.run_txa();
        self.run_and();
    }
    fn run_ahx(&mut self) {
        self.ins2 = self.regs.A.0 & self.regs.X.0 & self.ah;
    }
    fn run_tas(&mut self) {
        self.regs.S = self.regs.A & self.regs.X;
        self.ins2 = self.regs.S.0 & self.ah;
    }
    fn run_shy(&mut self) {
        self.ins2 = self.regs.Y.0 & self.ah;
    }
    fn run_shx(&mut self) {
        self.ins2 = self.regs.X.0 & self.ah;
    }
    fn run_ldy(&mut self) {
        self.regs.Y = Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_ldx(&mut self) {
        self.regs.X = Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_lda(&mut self) {
        self.regs.A = Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_lax(&mut self) {
        self.run_lda();
        self.run_ldx();
    }
    fn run_las(&mut self) {
        // Very useless garbage instruction
        self.ins2 &= self.regs.S.0;
        self.regs.A = Wrapping(self.ins2);
        self.regs.X = Wrapping(self.ins2);
        self.regs.S = Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn compare(&mut self, a: u8, b: u8) {
        let c = a.wrapping_sub(b);
        self.regs.P.set(PReg::Z, c == 0);
        self.regs.P.set(PReg::N, c >= 0x80);
        self.regs.P.set(PReg::C, c > a);
    }
    fn run_cmp(&mut self) {
        self.compare(self.regs.A.0, self.ins2);
    }
    fn run_cpx(&mut self) {
        self.compare(self.regs.X.0, self.ins2);
    }
    fn run_cpy(&mut self) {
        self.compare(self.regs.Y.0, self.ins2);
    }
    fn run_dcp(&mut self) {
        self.run_dec();
        self.run_cmp();
    }
    fn run_dec(&mut self) {
        self.ins2 = self.ins2.wrapping_sub(1);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_inc(&mut self) {
        self.ins2 = self.ins2.wrapping_add(1);
        self.regs.P.set(PReg::Z, self.ins2 == 0);
        self.regs.P.set(PReg::N, self.ins2 >= 0x80);
    }
    fn run_inx(&mut self) {
        self.ins2 = self.regs.X.0;
        self.run_inc();
        self.regs.X.0 = self.ins2;
    }
    fn run_iny(&mut self) {
        self.ins2 = self.regs.Y.0;
        self.run_inc();
        self.regs.Y.0 = self.ins2;
    }
    fn run_dex(&mut self) {
        self.ins2 = self.regs.X.0;
        self.run_dec();
        self.regs.X.0 = self.ins2;
    }
    fn run_dey(&mut self) {
        self.ins2 = self.regs.Y.0;
        self.run_dec();
        self.regs.Y.0 = self.ins2;
    }
    fn run_axs(&mut self) {
        self.regs.X = self.regs.X & self.regs.A;
        self.regs.P.set(PReg::C, self.regs.X.0 < self.ins2);
        self.regs.X -= Wrapping(self.ins2);
        self.regs.P.set(PReg::Z, self.regs.X.0 == 0);
        self.regs.P.set(PReg::N, self.regs.X.0 >= 0x80);
    }
    fn run_isc(&mut self) {
        self.run_inc();
        self.run_sbc();
    }
}
