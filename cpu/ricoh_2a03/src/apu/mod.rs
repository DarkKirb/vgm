pub mod n2a03;

pub trait APU: mem::MemoryMap {
    /// Returns the next sample
    fn get_sample() -> f32;
}
