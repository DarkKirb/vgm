//! Module for generating 2A03 audio

bitfield! {
    pub struct PulseMain(u8);
    impl Debug;
    pub envelope, set_envelope: 3, 0;
    pub bool, constant_volume, set_constant_volume: 4;
    pub bool, length_counter_halt, set_length_counter_halt: 5;
    pub duty, set_duty: 7, 6;
}
bitfield! {
    pub struct PulseSweep(u8);
    impl Debug;
    pub shift, set_shift: 2, 0;
    pub bool, negate, set_negate: 3;
    pub period, set_period: 6, 4;
    pub bool, enabled, set_enabled: 7;
}
bitfield! {
    pub struct PulseTimerLow(u8);
    impl Debug;
    pub timer_low, set_timer_low: 7, 0;
}
bitfield! {
    pub struct PulseTimerHigh(u8);
    impl Debug;
    pub timer_high, set_timer_high: 2, 0;
    pub length_counter, set_length_counter: 7, 3;
}

bitfield! {
    pub struct TriangleLinearCounter(u8);
    impl Debug;
    pub linear_counter, set_linear_counter: 6, 0;
    pub bool, counter_halt, set_counter_halt: 7;
}

bitfield! {
    pub struct NoiseEnvelope(u8);
    impl Debug;
    pub envelope, set_envelope: 3, 0;
    pub bool, constant_volume, set_constant_volume: 4;
    pub bool, envelope_loop, set_envelope_loop: 5;
}

bitfield! {
    pub struct NoiseSettings(u8);
    impl Debug;
    pub noise_period, set_noise_period: 3, 0;
    pub bool, loop_noise, set_loop_noise: 7;
}

bitfield! {
    pub struct NoiseLength(u8);
    impl Debug;
    pub length_counter, set_length_counter: 7, 3;
}

bitfield! {
    pub struct DMCControl(u8);
    impl Debug;
    pub frequency, set_frequency: 3, 0;
    pub bool, looping, set_loop: 6;
    pub bool, irq_enable, set_irq_enable: 7;
}

bitflags! {
    pub struct Status: u8 {
        /// Pulse channel 1
        const PULSE_1  = 0b0000_0001;
        /// Pulse channel 2
        const PULSE_2  = 0b0000_0010;
        /// Triangle Channel
        const TRIANGLE = 0b0000_0100;
        /// Noise Channel
        const NOISE    = 0b0000_1000;
        /// DMC Channel
        const DMC      = 0b0001_0000;
        /// Frame Interrupt
        const FRAME_INT= 0b0100_0000;
        /// DMC Interrupt
        const DMC_INT  = 0b1000_0000;
    }
}
