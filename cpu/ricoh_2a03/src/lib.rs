#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate bitfield;
extern crate mem;

pub mod cpu;
pub mod mmap;
pub mod apu;

bitflags! {
    pub struct AudioExpansions: u8 {
        /// Konami VRC6
        const VRC6 = 0b0000_0001;
        /// Konami VRC7
        const VRC7 = 0b0000_0010;
        /// Nintendo FDS
        const FDS  = 0b0000_0100;
        /// Nintendo MMC5
        const MMC5 = 0b0000_1000;
        /// Namco 163
        const N163 = 0b0001_0000;
        /// Sunsoft 5B
        const S5B  = 0b0010_0000;
    }
}


