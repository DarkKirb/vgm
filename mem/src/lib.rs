#![feature(try_from)]
use std::convert::TryInto;
use std::mem;

/// Trait used to convert integral types from and to an u8 array
pub trait Binarify: Sized + Copy {
    fn as_le(self) -> Vec<u8>;
    fn as_be(self) -> Vec<u8>;
    fn from_le(data: &[u8]) -> Self;
    fn from_be(data: &[u8]) -> Self;
}

impl Binarify for u8 {
    fn as_le(self) -> Vec<u8> {
        vec![self]
    }
    fn as_be(self) -> Vec<u8> {
        vec![self]
    }
    fn from_le(data: &[u8]) -> Self {
        data[0]
    }
    fn from_be(data: &[u8]) -> Self {
        data[0]
    }
}

impl Binarify for u16 {
    fn as_le(self) -> Vec<u8> {
        self.to_le_bytes().to_vec()
    }
    fn as_be(self) -> Vec<u8> {
        self.to_be_bytes().to_vec()
    }
    fn from_le(data: &[u8]) -> Self {
        u16::from_le_bytes(data.try_into().unwrap())
    }
    fn from_be(data: &[u8]) -> Self {
        u16::from_be_bytes(data.try_into().unwrap())
    }
}

impl Binarify for u32 {
    fn as_le(self) -> Vec<u8> {
        self.to_le_bytes().to_vec()
    }
    fn as_be(self) -> Vec<u8> {
        self.to_be_bytes().to_vec()
    }
    fn from_le(data: &[u8]) -> Self {
        u32::from_le_bytes(data.try_into().unwrap())
    }
    fn from_be(data: &[u8]) -> Self {
        u32::from_be_bytes(data.try_into().unwrap())
    }
}

impl Binarify for u64 {
    fn as_le(self) -> Vec<u8> {
        self.to_le_bytes().to_vec()
    }
    fn as_be(self) -> Vec<u8> {
        self.to_be_bytes().to_vec()
    }
    fn from_le(data: &[u8]) -> Self {
        u64::from_le_bytes(data.try_into().unwrap())
    }
    fn from_be(data: &[u8]) -> Self {
        u64::from_be_bytes(data.try_into().unwrap())
    }
}

pub trait MemoryMap {
    fn read_byte(&mut self, addr: u32) -> Option<u8>;
    fn write_byte(&mut self, addr: u32, data: u8) -> Option<()>;

    fn read_little<T2>(&mut self, addr: u32) -> Option<T2>
    where
        T2: Binarify + Sized,
    {
        let mut data = vec![0_u8; mem::size_of::<T2>()];
        for i in 0..data.len() {
            data[i] = self.read_byte(addr + i as u32)?;
        }
        Some(T2::from_le(&data))
    }
    fn read_big<T2>(&mut self, addr: u32) -> Option<T2>
    where
        T2: Binarify + Sized,
    {
        let mut data = vec![0_u8; mem::size_of::<T2>()];
        for i in (0..data.len()).rev() {
            // Access order may matter!
            data[i] = self.read_byte(addr + i as u32)?;
        }
        Some(T2::from_be(&data))
    }
    fn write_little<T2: Binarify>(&mut self, addr: u32, data: T2) -> Option<()> {
        let data = data.as_le();
        for (i, c) in data.iter().enumerate() {
            self.write_byte(addr + i as u32, *c)?;
        }
        Some(())
    }
    fn write_big<T2: Binarify>(&mut self, addr: u32, data: T2) -> Option<()> {
        let data = data.as_le();
        for (i, c) in data.iter().enumerate().rev() {
            self.write_byte(addr + i as u32, *c)?;
        }
        Some(())
    }
}
